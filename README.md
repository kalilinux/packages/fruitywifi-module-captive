module_captive
==============

FruityWifi captive portal module


v1.8
---------------------------------
- PHP7 compatibility issues has been fixed


v1.6
---------------------------------
- FruityWifi v2.4 is required
- New options tab has been added
- Captive portal template can be changed
- Recon option has been added (captures browser details and plugins)
- Inject Code option has been added
- CONNTRACK has been removed


v1.0
---------------------------------
- FruityWifi v1.5 is required
- CONNTRACK needs to be installed manually (apt/tar)


REF: http://www.andybev.com/index.php/Using_iptables_and_PHP_to_create_a_captive_portal
REF: http://aryo.info/labs/captive-portal-using-php-and-iptables.html
